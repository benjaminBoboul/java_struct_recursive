package imt.etu.benjamin.recurse;

public class main {
    // 6.1)
    public static int C(int n, int k) {
        if(k == 0) return 1;
        if(k == n) return 1;
        if(k > n) return 0;
        else if(0 < k) return C(n-1, k-1) + C(n-1, k);
        return 0;
    }

    public static void main(String[] args) {
        for (int i=0; i < 11; i++)
            System.out.println(C(i, (int) (i*Math.random())));
    }
}
