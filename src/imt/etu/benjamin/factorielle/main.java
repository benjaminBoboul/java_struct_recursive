package imt.etu.benjamin.factorielle;

public class main {
    private static int fact(int trigger) {
        if (trigger <= 0) return 1;
        else return trigger*fact(trigger-1);
    }

    private static int interactiveFact(int trigger) {
        int result=1;
        for (int i = 2; i < trigger+1; i++) result*=i;
        return result;
    }
    public static void main(String[] args) {
        for (int i = 0; i < 5; i++)
            System.out.println("Fact recursive : "+fact(i)+
                    ", Fact interactive : "+interactiveFact(i));
    }
}
